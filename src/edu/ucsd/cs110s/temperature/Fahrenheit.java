package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
  public Fahrenheit(float t)
  {
	  super(t);
  }
  public String toString()
  {
	  //TODO Complete this method
	  return this.value +""; //"Temperature: " + this.value + " degrees Fahrenheit";
  }
  @Override
  public Temperature toFahrenheit() {
  	// TODO Auto-generated method stub
  	Temperature temp = this;
  	temp.value = (float) (temp.value * 1.8);
  	temp.value = temp.value + 32;
  	return temp;
  }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	Temperature temp = this;
	temp.value = temp.value - 32;
	temp.value = (float) (temp.value / 1.8);
	return temp;
}
  
}
