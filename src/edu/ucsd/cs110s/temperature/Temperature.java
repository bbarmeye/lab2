/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author bbarmeye
 *
 */
public abstract class Temperature {
  float value;
  public Temperature(float v)
  {
	  value = v;
  }
  public final float getValue()
  {
	  return value;
  }
  public abstract Temperature toCelsius();
  public abstract Temperature toFahrenheit();
}
